
1. Clone this repo to your machine- `git clone https://ys12@bitbucket.org/yuvalsha12/week6_ansible.git`.
2. [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu)
3. Run the command `export ANSIBLE_HOST_KEY_CHECKING=False` to enable connection to the hosts machines with user name and password.
4. Add the load balancer public ip to the okta app sign-in redirect URIs.
5. Add the file `inventory`. example:
   fill it with `.tfvars` files and your Terraform output
    ```
    <vm1 ip> host="<vm1 ip>"
    <vm2 ip> host="<vm2 ip>"
    ...

6. Add the file `vars` in the project root. example:
    ```
    # the host variable is in the inventory file
    pghost: "<your postgresql service address>
    pg_username: "<postgresql username>"
    pg_password: "<postgresql password>"
    LB_ip: "<your load balancer public ip address>"
    okta_url: "<your okta url>"
    okta_client_id: "<your okta client id>"
    okta_client_secret: "<your okta client secret>"
    ansible_connection: "ssh"
    ansible_port: "22"
    ansible_user: "<user name>" 
    ansible_ssh_pass: "<password>"

7. Run `ansible-playbook -i inventory play_book.yaml`.